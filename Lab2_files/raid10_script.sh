#!/usr/bin/bash

		######Sobiraem#########RAID10##########
	sudo mdadm --zero-superblock --force /dev/sd{b,c,d,e,f,g}
	sudo mdadm --create --auto=yes --verbose /dev/md0 -l 10 -n 6 /dev/sd{b,c,d,e,f,g}
	
		######Sobiraem mdadm.conf#############
	sudo mkdir -p /etc/mdadm
	sudo bash -c "echo 'DEVICE partitions' > /etc/mdadm/mdadm.conf"
	sudo bash -c "mdadm --detail --scan --verbose | awk '/ARRAY/ {print}' >> /etc/mdadm/mdadm.conf"


		#######Sozdadim GPT na RAID_10########
	sudo parted -s /dev/md0 mklabel gpt
	
		#######Sozdadim 5 razdelov po 20%#####
	n=20; for i in $(seq 0 20 80); do sudo parted -a opt /dev/md0 mkpart primary ext4 $i% $(($n+$i))%; done; unset n

		#######Sozdadim na 5 razdelah 5 failovih sistem ext4
	for i in $(seq 1 5); do sudo mkfs.ext4 /dev/md0p$i; done

		#######Sozdadim tochki montirovaniya dlya 5 razdelov ext4
		sudo mkdir -p /raid/part{1..5}
		#######Smontiruem 5 razdelov na 5 tochek montirovaniya i dobavim zapisi v /etc/fstab
		for i in $(seq 1 5); do sudo mount /dev/md0p$i /raid/part$i; done
		#######Vnesem novie dannie o razdelah v /etc/fstab
		for i in $(seq 1 5); do sudo bash -c "echo -e '`sudo blkid /dev/md0p$i | awk '{print $2}' | sed 's/"//g'`\t/raid/part$i\text4\tdefaults\t0\t0' >> /etc/fstab" ;done

