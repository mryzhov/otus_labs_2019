#!/usr/bin/bash
#Запись надо добавить в Крон для ежечасного запуска. crontab -e 0 * * * * /home/vagrant/ng_log_analyser.sh

#Проверка на наличие файла-счётчика строк. Это значение мы учитываем при каждом запуске скрипта.
#Если такого файла нет, создаем его с записью 1 (обычно при первом запуске).
if [[ ! -f ./pv-lines.txt ]]
         then
                 echo "1" > ./pv-lines.txt
fi
#Считываем в переменную путь к файлу с которым будем работать.
NGINXFILE='/var/log/nginx/nginx.log'
#Путь к файлу блокировки повторного запуска.
BLOCKFILE=/$HOME/blockfile
#Сохраняем в переменную текущую дату.
TODAY=$(date)
#Файл счёта строк. Каждый раз обновляем значение после окончания анализа логов.
PV_NL=$(cat ./pv-lines.txt)

#Проверка попытки повтороного запуска сценария. Если файл blockfile существует, повторного запуска не будет. Если файла нет, создаем его и сохраняем туда PID текущего процесса.
if [ -f $BLOCKFILE ]
then
  echo "Lockfile exist, $(cat /$BLOCKFILE) is running, no new runs."
  exit 1 
  trap 'rm -f $BLOCKFILE"; exit $?' INT TERM EXIT
else
  echo "PID: $$" > $BLOCKFILE
fi
#Функция поиска Find. Если нет файла лога или он вне доступа, вызываем функцию и смотрим файлы в /var/log/. Список отправляем на почту.
poisk(){
find /var/log/ -type f -exec ls {} \; 2>/dev/null > ./find.log
cat ./find.log | mail -s "No file. Report of searching Nginx Logs /var/log/" vagrant
rm -f $BLOCKFILE
exit 0
trap - INT TERM EXIT
}
# Часть повторяющегося кода вынес в функцию
sortirovka(){
sort | uniq -c | sort -rn
}

#Проверяем есть ли файл лога Nginx с которым мы собираемся работать.
if [[ ! -f $NGINXFILE ]]
         then
                 poisk
fi

# Анализ лога nginx.log

#Считаем максимальное кол-во запросов с момента последнего запуска для 10и ip адресов
awk -v pvnl="$PV_NL" '(NR > pvnl) {print $1}' $NGINXFILE | sortirovka | head > ipuniq.txt
#считаем  кол-во запрашиваемых адресов с момента последнего запуска
awk -v pvnl="$PV_NL" '(NR > pvnl) {print $7}' $NGINXFILE | sortirovka | head > ipquery.txt
# считаем кол-во ошибок
awk -v pvnl="$PV_NL" '(NR > pvnl) {print $9}' $NGINXFILE |grep -E "[4-5]{1}[0-9][[:digit:]]" | sortirovka  > errors.txt
#считаем список кодов возвратов с их общим количеством
awk -v pvnl="$PV_NL" '(NR > pvnl) {print $9}' $NGINXFILE | sortirovka  > code_all.txt

# считаем количество строк в логе и записываем значение в файл, чтоб через час начать с добавившихся строк.
awk 'END{print NR}' $NGINXFILE > ./pv-lines.txt
# Убираем блокировку повторного запуска.
rm -f $BLOCKFILE

# Отправка отчета о работе скрипта на почту.
trap 'echo -e "Today is $(echo $TODAY)\n\nCount of ip:\n\n$(cat ./ipuniq.txt)\n\nCount of queries:\n\n$(cat ./ipquery.txt)\n\nCount of errors:\n\n$(cat ./errors.txt)\n\nCount of cods:\n\n$(cat ./code_all.txt)" | mail -s "Report of analysing Nginx Log" vagrant' EXIT
