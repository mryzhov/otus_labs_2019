#!/bin/bash

#0.Path ot /proc/ directory
PROC_PATH='/proc/' 
#1.Container of the list of processes
SYS_PID=$(ls $PROC_PATH | grep -E '[[:digit:]]' | sort -n | xargs)

####################

function psax {
#2.Going to the PROC directory
cd $PROC_PATH

    for i in $SYS_PID
    do
#3.Analizing "/proc/$SYS_PID/stat", "/proc/$SYS_PID/environ","/proc/$SYS_PID/fd/"
        if [[ -e $i/environ && -e $i/stat ]]; then
            
#4.Get VALUES
#5.Get value of field PID
            FieldPid=$(cat $i/stat | awk '{ printf "%7d", $1 }')

#6.Define the terminal Field TTY
            User_TTY=$(awk '{print $7}' $i/stat)

#7.Status of the field STATE
            FS=$(cat $i/stat | awk '{ printf "%1s", $3 }')
            SL=$(cat $i/stat | awk '{ printf $6 }')
            Ni=$(cat $i/stat | awk '{ printf $19 }')
            Threads=$(cat $i/stat | awk '{ printf $20 }')
            VmemPages=$(grep VmFlags $i/smaps | grep lo)
            ProcFG=$(cat $i/stat | awk '{ printf $8 }')
#8.Define the stroke of the field COMMAND
            FCmd=$(cat $i/cmdline | awk '{ printf "%10s", $1 }')
#9.Field STAT
            ## Flag 'l'(threads)
            [[ $Threads -gt 1 ]] && Threads='l' || Threads=''
            ## Flag 'L'(vm lock pages)
            [[ -n $VmemPages ]] && VmemPages='L' || VmemPages=''
            ## Flag '+' foreground process group
            [[ $ProcFG -eq "-1" ]] && ProcFG='' || ProcFG='+'
            ## Flag 's' session leader
            [[ $SL -eq $i ]] && SL='s' || SL=''
            ## Flag '<' or 'N' priority (the lowest is 20 and the highest is -19)
            if [[ $Ni -lt 0 ]]; then Ni='<'; elif [[ $Ni -gt 0 ]]; then Ni='N'; else Ni=''; fi
            
            # Field TTY
            # Flag 'pts|tty' or User_TTY - terminal tty which uses process (pseudo terminal slave and teletype)
            [[ User_TTY -eq 0 ]] && TTY='?' || TTY=$(ls -l $i/fd/ | grep -E 'tty|pts' | cut -d\/ -f3,4 | uniq)
            
#10. Field COMMAND, second try to find command in file /proc/pid/stat
            [[ -z $FCmd ]] && FCmd=`cat $i/stat | awk '{ printf "%10s", $2 }' | tr '(' '[' | tr ')' ']'`
        fi
#11. Result of function psax
        # 
        Result="$FS$Ni$SL$Threads$VmemPages$ProcFG"
#12. Collecting the line of output
        printf "%5d %-6s %-7s %s\n" "$FieldPid" "$TTY" "$Result" "$FCmd"
    done
}

printf "%5s %-6s %-7s %s\n" "PID" "TTY" "STAT" "COMMAND"

psax
